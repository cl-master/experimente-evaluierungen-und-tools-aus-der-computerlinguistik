#!/usr/bin/env python3

import sys
import math
from collections import defaultdict
import random
import pickle
import numpy

#training_file = sys.argv[1]
training_file = 'Tiger/train.txt'
paramfile = 'paramfile'


class TrainingTagger:
    def __init__(self, train_file, max_ffix, epochs, learning_rate):
        self.max_ffix = max_ffix
        self.weight = defaultdict(float)
        print('preparing data......')
        self.dataset = self.read_data(train_file)
        self.tagset = {tag for _, tags in self.dataset for tag in tags}
        print('start training...')
        for n in range(epochs):
            self.train_on_sent(learning_rate)
            print(n+1,' iteration finished')

    def read_data(self, file):
        dataset, words, tags = [], [], []
        with open(file, 'r') as f:
            for line in f:
                if line != '\n':
                    words.append(line.split()[0])
                    tags.append(line.split()[1])
                else:
                    words.append(' ')
                    tags.append('<s>')
                    dataset.append((words, tags))
                    words, tags = [], []
            if words and tags:  # add the last sentence in case there are no \n at the end of file
                words.append(' ')
                tags.append('<s>')
                dataset.append((words, tags))
        return dataset

    def extract_features(self, prevtag, tag, words, pos):
        features = []
        feature_WT = 'WT-' + words[pos] + '-' + tag
        features.append(feature_WT)
        for n in range(2, min(self.max_ffix, len(words[pos])) + 1):
            feature_ST = 'ST-' + words[pos][-n:] + '-' + tag
            feature_PT = 'PT-' + words[pos][:n] + '-' + tag
            features.extend((feature_ST, feature_PT))
        if pos > 0:
            feature_TW2 = 'TW2-' + tag + '-' + words[pos - 1]
            feature_TT = 'TT-' + prevtag + '-' + tag
            feature_TTW = 'TTW-' + prevtag + '-' + tag + '-' + words[pos]
            feature_TTW_1 = 'TTW_1-' + prevtag + '-' + tag + '-' + words[pos - 1]
            features.extend((feature_TW2, feature_TT, feature_TTW,feature_TTW_1))
        return features

    def lex_and_context_score(self, features):
        return sum(self.weight[feature] for feature in features)

    def forward(self, words):
        forward = defaultdict(lambda: defaultdict(float))
        forward[-1]['<s>'] = 1
        for pos in range(len(words)):
            for tag in self.tagset:
                for prevtag in self.tagset:
                    forward[pos][tag] += numpy.logaddexp(self.lex_and_context_score(
                        self.extract_features(prevtag,tag,words,pos)),sum(forward[pos-1].values()))
        return forward

    def backward(self, words):
        backward = defaultdict(lambda: defaultdict(float))
        backward[len(words)]['<s>'] = 1

        for pos in range(len(words)-1,-1,-1):
            for tag in self.tagset:
                for prevtag in self.tagset:
                    backward[pos][tag] += numpy.logaddexp(self.lex_and_context_score(
                        self.extract_features(prevtag, tag, words, pos)), sum(backward[pos+1].values()))
        return backward

    def expected_freqs(self, words):
        expected_freq = defaultdict(lambda: defaultdict(float))
        forward = self.forward(words)
        backward = self.backward(words)
        for pos in range(len(words)):
            for tag in self.tagset:
                for prevtag in self.tagset:
                    expected_freq[pos][tag] = forward[pos][tag]*backward[pos][tag]/forward[len(words)-1]['<s>']
                    + forward[pos-1][tag]*self.lex_and_context_score(self.extract_features(prevtag,tag,words,pos))*backward[pos][prevtag]\
                    /forward[len(words)-1]['<s>']
        return sum(v for pos, dictionary in expected_freq.items() for k,v in dictionary.items())

    def observed_freqs(self, words, tags):
        features = []
        for pos in range(len(words)):
            prevtag = tags[pos-1] if pos-1 >= 0 else None
            tag = tags[pos]
            feature = self.extract_features(prevtag,tag,words,pos)
            features.extend(feature)
        return features

    def train_on_sent(self, learning_rate):
        for words, tags in self.dataset:
            exp_freq = self.expected_freqs(words)
            obs_features = self.observed_freqs(words,tags)
            obs_freq = len(obs_features)
            gradient = obs_freq - exp_freq
            for feature in obs_features:
                self.weight[feature] += learning_rate
            for feature in self.weight:
                self.weight[feature] -= learning_rate * gradient
                print(feature, self.weight[feature])

    # save parameters
    def save_parameters(self, filename):
        params = dict()
        params['tagset'] = self.tagset
        params['weight'] = self.weight
        params['ffix_length'] = self.max_ffix
        with open(filename, 'wb') as file:
            pickle.dump(params, file)
            print('Parameter saved!')


if __name__ == '__main__':
    tagger = TrainingTagger(training_file, 5, 5, 0.1)
    tagger.save_parameters(paramfile)
