#!/usr/bin/env python3

import sys
import math
from collections import defaultdict
import random
import pickle
import numpy

#training_file = sys.argv[1]
training_file = 'Tiger/train.txt'
paramfile = 'paramfile'


class TrainingTagger:
    def __init__(self, train_file, max_ffix, epochs, learning_rate, l2_faktor):
        self.max_ffix = max_ffix
        self.weight = defaultdict(float)
        self.features = []
        self.tagset = set()
        print('reading data...')
        self.read_data(train_file, max_ffix)
        print('start training...')
        for n in range(epochs):
            random.shuffle(self.features)
            for sentence in self.features:
                self.update_weight(sentence, learning_rate, l2_faktor)
            print(n + 1, ' iteration finished')
        print(self.weight)

    def read_data(self, file, max_ffix):
        dataset, words, tags = [], [], []
        with open(file, 'r') as f:
            for line in f:
                if line != '\n':
                    words.append(line.split()[0])
                    tags.append(line.split()[1])
                else:
                    words.append(' ')
                    tags.append('<s>')
                    dataset.append((words, tags))
                    words, tags = [], []
            if words and tags:  # add the last sentence in case there are no \n at the end of file
                words.append(' ')
                tags.append('<s>')
                dataset.append((words, tags))
        return self.extract_features(dataset, max_ffix)

    def extract_features(self, data, length):
        for words, tags in data:
            features = defaultdict(lambda: defaultdict(list))
            for i in range(len(words)):
                self.tagset.add(tags[i])
                feature_WT = 'WT-' + words[i] + '-' + tags[i]
                features[i]['lexical'].append(feature_WT)
                self.weight[feature_WT] = 0
                for n in range(2, min(length, len(words[i]))+1):
                    feature_ST = 'ST-' + words[i][-n:] + '-' + tags[i]
                    feature_PT = 'PT-' + words[i][:n] + '-' + tags[i]
                    features[i]['lexical'].append(feature_ST)
                    features[i]['lexical'].append(feature_PT)
                if i > 0:
                    feature_TW2 = 'TW2-' + tags[i] + '-' + words[i-1]
                    feature_TT = 'TT-' + tags[i-1] + '-' + tags[i]
                    feature_TTW = 'TTW-' + tags[i-1] + '-' + tags[i] + '-' + words[i]
                    feature_TTW_1 = 'TTW_1-' + tags[i-1] + '-' + tags[i] + '-' + words[i-1]
                    features[i]['lexical'].append(feature_TW2)
                    features[i]['context'].append(feature_TT)
                    features[i]['context'].append(feature_TTW)
                    features[i]['context'].append(feature_TTW_1)
            self.features.append(features)

    def lex_or_context_score(self, features):
        return numpy.logaddexp(sum(self.weight[feature] for feature in features['lexical']),sum(self.weight[feature] for feature in features['context']))

    def forward(self, sentence):
        forward = defaultdict(float)
        forward[-1] = 1
        for pos, features_dict in sentence.items():
            forward[pos] = forward[pos-1] * self.lex_or_context_score(features_dict)
        return forward

    def backward(self, sentence):
        backward = defaultdict(float)
        backward[len(sentence)] = 1
        for pos, features_dict in sorted(sentence.items(), key=lambda x: -x[0]):
            backward[pos] = backward[pos+1] * self.lex_or_context_score(features_dict)
        return backward

    def update_weight(self, sentence, learning_rate, l2):
        expected_freq = dict()
        forward = self.forward(sentence)
        backward = self.backward(sentence)
        for pos, f_score in forward.items():
            expected_freq[pos] = f_score*backward[pos]/forward[-1] + \
                                 forward[max(pos-1,0)]*self.lex_or_context_score(sentence[pos])*backward[pos]/forward[-1]
        gradient = sum(len(feature_list) for feature_list in sentence.values()) - sum(expected_freq.values())
        for feature_list in sentence.values():
            for feature in feature_list:
                self.penalty_l2(feature, gradient, learning_rate, l2)

    def penalty_l2(self, feature, gradient, learning_rate, mu):
        self.weight[feature] = self.weight[feature]*(1-learning_rate*mu)+learning_rate*gradient

    # save parameters
    def save_parameters(self, filename):
        params = dict()
        params['tagset'] = self.tagset
        params['weight'] = self.weight
        params['ffix_length'] = self.max_ffix
        with open(filename, 'wb') as file:
            pickle.dump(params, file)
            print('Parameter saved!')


if __name__ == '__main__':
    tagger = TrainingTagger(training_file, 5, 5, 0.001, 1)
    tagger.save_parameters(paramfile)


