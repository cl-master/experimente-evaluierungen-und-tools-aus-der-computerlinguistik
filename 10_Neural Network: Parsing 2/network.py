#!/usr/bin/env python3

import torch
import torch.nn as nn


class WordEncoder(nn.Module):
    def __init__(self, charVocabSize, charEmbSize, charLSTMSize, dropout):
        super().__init__()
        self.char_embed = nn.Embedding(charVocabSize, charEmbSize)
        self.char_LSTM = nn.LSTM(charEmbSize, charLSTMSize, bidirectional=False, batch_first=True)
        self.drop = nn.Dropout(dropout)

    def forward(self, prefixes, suffixes):
        pre = self.char_embed(prefixes)
        suff = self.char_embed(suffixes)
        pre_lstm, _ = self.char_LSTM(self.drop(pre))
        suff_lstm, _ = self.char_LSTM(self.drop(suff))
        word_vec = torch.cat([pre_lstm[:, -1], suff_lstm[:, -1]], dim=1)  # concatenate the last state of two LSTMs
        return word_vec


class SentEncoder(nn.Module):
    def __init__(self, charLSTMSize, wordLSTMSize, numLayers, dropout):
        super().__init__()
        self.word_LSTM = nn.LSTM(charLSTMSize*2, wordLSTMSize, num_layers=numLayers, bidirectional=True, dropout=dropout)
        self.drop = nn.Dropout(dropout)

    def forward(self, wordRepr):
        wordRepr = torch.unsqueeze(wordRepr, dim=1)
        sent, _ = self.word_LSTM(self.drop(wordRepr))
        sent = torch.squeeze(sent, dim=1)
        return sent


class SpanEncoder(nn.Module):
    def __init__(self, dropout):
        super().__init__()
        self.drop = nn.Dropout(dropout)

    def forward(self, sentPosRepr):
        # split the concatenation of the forward- und backward-states
        forwardRepr, backwardRepr = torch.chunk(self.drop(sentPosRepr), 2, dim=1)
        all_span = []
        for l in range(1, sentPosRepr.size(0)-1):
            forward = forwardRepr[l:-1] - forwardRepr[:-l-1]
            backward = backwardRepr[1:-l] - backwardRepr[1+l:]
            span = torch.cat([forward, backward], dim=1)
            all_span.append(span)
        all_span = torch.cat(all_span, dim=0)  # concatenate all the spans
        return all_span


class OutputLayer(nn.Module):
    def __init__(self, spanReprSize, hiddenSize, numCategories, dropout):
        super().__init__()
        self.Linear = nn.Linear(spanReprSize*2, hiddenSize)
        self.Linear2 = nn.Linear(hiddenSize, numCategories)
        self.relu = nn.ReLU()
        self.drop = nn.Dropout(dropout)

    def forward(self, spanRepr):
        out = self.Linear(self.drop(spanRepr))
        out = self.relu(out)  # use ReLu as intermediate layer
        score = self.Linear2(self.drop(out))
        score[:, 0] = 0  # Die Bewertung des Labels “keine Konstituente” (mit Label-ID 0) sollte auf 0 gesetzt werden.
        return score


if __name__ == '__main__':
    charVocabSize = 30
    charEmbSize = 50
    charLSTMSize = 50
    numLayers = 2
    wordLSTMSize = 100
    hiddenSize = 100
    numCategories = 25
    dropout = 0.3

    # initiate test tensors
    preffix = torch.randint(0, 30, (6, 10), dtype=torch.long)
    suffix = torch.randint(0, 30, (6, 10), dtype=torch.long)

    # initiate neuron networks
    wordencoder = WordEncoder(charVocabSize, charEmbSize, charLSTMSize, dropout)
    sentencoder = SentEncoder(charLSTMSize, wordLSTMSize, numLayers, dropout)
    spanencoder = SpanEncoder(dropout)
    output = OutputLayer(wordLSTMSize, hiddenSize, numCategories, dropout)

    # feedforward
    word_re = wordencoder(preffix, suffix)
    sent_re = sentencoder(word_re)
    span_re = spanencoder(sent_re)
    score = output(span_re)
    print(score.size())

