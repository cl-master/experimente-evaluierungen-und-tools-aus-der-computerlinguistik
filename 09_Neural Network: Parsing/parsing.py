#!/usr/bin/env python3

# parsing a single tree
def read_tree(tree):
    left_index, tags, tags_span, words = [], [], [], []
    #  get the index span for each tag or tag+word and save it into tags_span
    # e.g.: [('S', 4, 65), ('TOP', 0, 66), ('NNP Ms.',9,17)]
    tag = ''
    for pos in range(len(tree)):
        if tree[pos] == '(':
            left_index.append(pos)
            if tag:
                tags.append(tag)
                tag = ''
        elif tree[pos] == ')':
            if tag:
                tags.append(tag)
                tag = ''
            if not tags or not left_index:
                raise Exception('invalid tree structure!')
            tags_span.append((tags.pop(), left_index.pop(), pos))
        else:
            tag += tree[pos]
    if left_index or tags:
        raise Exception('invalid tree structure!')
    # extract words from tags
    for tag_word, start, end in tags_span:
        if ' ' in tag_word:
            tag, word = tag_word.split()
            words.append((word, start, end))
            tags.append((tag, start, end))
        else:
            tags.append((tag_word, start, end))
    tags = sorted(tags,key=lambda x:x[1])
    # add the word index to its corresponding tags
    constituent = []
    for tag, start, end in tags:
        constituent.append([tag, len(words), 0])
        for word, start_2, end_2 in words:
            if start <= start_2:
                constituent[-1][1] -= 1
            if end >= end_2:
                constituent[-1][2] += 1
    # combine the tags having same word index with '='
    constituent_dict = {}
    for tag, start, end in constituent:
        if (start,end) in constituent_dict:
            constituent_dict[(start,end)] += '='+tag
        else:
            constituent_dict[(start, end)] = tag
    constituent = [(tag, start, end) for (start, end), tag in constituent_dict.items()]
    return [word[0] for word in words], constituent

# generate a tree from word list and constituent list
def generate_tree(words, constituent):
    constituent1 = []
    # split the tags combined before
    for tag, start, end in constituent:
        if '=' in tag:
            tags = tag.split('=')
            for t in tags:
                constituent1.append((t,start,end))
        else:
            constituent1.append((tag,start,end))
    tree = ''
    for pos in range(len(words)):
        for tag, start, _ in constituent1:
            if start == pos:
                tree += '(' + tag
        tree += ' '+words[pos]
        for _, _, end in constituent1:
            if end == pos+1:
                tree += ')'
    return tree


if __name__ == '__main__':
    tree = "(TOP(S(SBAR(WHNP(WP What))(S(NP(NP(DT the)(NNS investors))(SBAR(WHNP(WP who))(S(VP(VBP oppose)(NP(DT the)(VBN proposed)(NNS changes))))))(VP(VBP object)(PP(TO to))(ADVP(RBS most)))))(VP(VBZ is)(NP(NP(DT the)(NN effect))(SBAR(S(NP(PRP they))(VP(VBP say)(SBAR(S(NP(DT the)(NN proposal))(VP(MD would)(VP(VB have)(PP(IN on)(NP(PRP$ their)(NN ability)(S(VP(TO to)(VP(VB spot)(NP(NP(NP(JJ telltale)(`` ``)(NNS clusters)('' ''))(PP(IN of)(NP(NN trading)(NN activity))))(: --)(NP(NP(VBG buying)(CC or)(NN selling))(PP(IN by)(NP(QP(JJR more)(IN than)(CD one))(NN officer)(CC or)(NN director)))(PP(IN within)(NP(NP(DT a)(JJ short)(NN period))(PP(IN of)(NP(NN time)))))))))))))))))))))(. .)))"
    words, cons = read_tree(tree)
    generated_tree = generate_tree(words,cons)
    print(words, cons, generated_tree, sep='\n')


