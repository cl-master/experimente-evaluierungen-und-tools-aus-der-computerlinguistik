#!/usr/bin/env python3

import sys
import pickle

testfile = sys.argv[1]
paramfile = sys.argv[2]
# testfile = './test/spam/0002.2004-08-01.BG.spam.txt'
# paramfile = 'paramfile'

# read parameters
with open(paramfile, 'rb') as file:
    weight_table = pickle.load(file)

# calculate score of two classes for a test file and return predicted label
def calculate_score(features):
    score = dict()
    for c in ['spam','ham']:
        score[c] = sum(weight_table.get(feature+'-'+c, 0) for feature in features)
    label = max(score.keys(), key=score.get)
    return label


# read file to be annotated and extract features
with open(testfile, 'r', encoding='ISO-8859-1') as m:
    txt = m.read().split()
    features = []
    for i in range(len(txt)):
        features.append(txt[i])
        if i < len(txt) - 1:
            bigram = ' '.join(txt[i:i + 2])
            features.append(bigram)
predict_label = calculate_score(features)
print('The predicted label of {} is {}'.format(testfile, predict_label))
