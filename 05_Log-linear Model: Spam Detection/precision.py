#!/usr/bin/env python3

import sys
import pickle
import os

testdir = sys.argv[1]
paramfile = sys.argv[2]
# testdir = './test'
# paramfile = 'paramfile'

# read parameters
with open(paramfile, 'rb') as file:
    weight_table = pickle.load(file)


# calculate score of two classes for a test file and return predicted label
def calculate_score(features):
    score = dict()
    for c in ['spam','ham']:
        score[c] = sum(weight_table.get(feature+'-'+c, 0) for feature in features)
    label = max(score.keys(), key=score.get)
    return label


# read test data, predict label and calculate precision
correct = 0
mail_list = [root + '/' + file for root, _, files in os.walk(testdir) for file in files]
for mail in mail_list:
    with open(mail, 'r', encoding='ISO-8859-1') as m:
        txt = m.read().split()
        features = []
        for i in range(len(txt)):
            features.append(txt[i])
            if i < len(txt) -1:
                bigram = ' '.join(txt[i:i+2])
                features.append(bigram)
    predict_label = calculate_score(features)
    correct += 1 if predict_label in mail else 0
    print(mail, predict_label)

precision = correct/len(mail_list)
print('total files: {}; correct classified: {}'.format(len(mail_list), correct))
print('precision: {}'.format(correct/len(mail_list)))
