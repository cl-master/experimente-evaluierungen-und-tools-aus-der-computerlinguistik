#!/usr/bin/env python3

import os
from collections import defaultdict
import math
import pickle
import random
import argparse

parser = argparse.ArgumentParser(description='Training log linear model...')
parser.add_argument("traindir", help="provide the root directory of training files")
parser.add_argument('paramfile', help='provide a parameter file saving the trained parameters')
parser.add_argument('-e', '--epochs', type=int, help='times for training iteration', default=5)
parser.add_argument('-l', '--learning_rate', type=float, help='learning_rate', default=0.9)
parser.add_argument('-m', '--l2_factor', type=float, help='factor for l2 penalty', default=0.99999)
args = parser.parse_args()
# traindir = './train'
# paramfile = 'paramfile'


class LogLinearModel:
    def __init__(self, train_dir, epochs, learning_rate, l2_faktor):
        self.weight = defaultdict(float)
        print('reading data...')
        training_data = self.read_data(train_dir)
        print('start training...')
        for n in range(epochs):
            random.shuffle(training_data)
            for data in training_data:
                self.update_feature_weight(data, learning_rate, l2_faktor)
            print(n+1, ' iteration finished')

    # read training data and extract unigrams & bigrams as features
    # initialize the weight dictionary with default value 0
    def read_data(self, directory):
        mail_list = [root + '/' + file for root, _, files in os.walk(directory) for file in files]
        mails = []
        for mail in mail_list:
            with open(mail, 'r', encoding='ISO-8859-1') as m:
                txt = m.read().split()
                features = []
                for i in range(len(txt)):
                    tok = txt[i] + '-spam' if 'spam' in mail else txt[i] + '-ham'
                    features.append(tok)
                    self.weight[txt[i] + '-spam'] = 0
                    self.weight[txt[i] + '-ham'] = 0
                    if i < len(txt) - 1:
                        bigram = ' '.join(txt[i:i+2]) + '-spam' if 'spam' in mail else ' '.join(txt[i:i+2]) + '-ham'
                        features.append(bigram)
                        self.weight[' '.join(txt[i:i+2]) + '-spam'] = 0
                        self.weight[' '.join(txt[i:i+2]) + '-ham'] = 0
                mails.append(features)
        return mails

    # calculate the gradient for each single feature and then update the weight one by one
    def update_feature_weight(self, feature_list, learning_rate, l2):
        for feature in feature_list:
            tok = feature.split('-')[0]
            label = feature.split('-')[-1]
            spam_score, ham_score = self.normalized_score(tok)
            if label == 'spam':
                expect_freq = spam_score  # 1 * spam_score + 0 * ham_score
            else:
                expect_freq = ham_score  # 0 * spam_score + 1 * ham_score
            gradient_1 = 1 - expect_freq
            self.penalty_l2(feature, gradient_1, learning_rate, l2)  # update the current active feature
            gradient_2 = 0 - expect_freq
            feature_2 = tok + '-ham' if label == 'spam' else tok + '-spam'
            self.penalty_l2(feature_2, gradient_2, learning_rate, l2) # update the inactive feature

    # calculate the score for a single feature in each class
    def normalized_score(self, token):
        spam_score = math.exp(self.weight[token + '-spam'])  # freq is 1
        ham_score = math.exp(self.weight[token + '-ham'])  # freq is 1
        z = ham_score + spam_score
        return spam_score/z, ham_score/z

    # L2 regularization
    def penalty_l2(self, feature, gradient, learning_rate, mu):
        self.weight[feature] = self.weight[feature]*(1-learning_rate*mu)+learning_rate*gradient

    # save parameters
    def save_parameters(self, filename):
        with open(filename, 'wb') as file:
            pickle.dump(self.weight, file)
            print('Parameter saved!')


if __name__ == '__main__':
    classifier = LogLinearModel(args.traindir, args.epochs, args.learning_rate, args.l2_factor)
    classifier.save_parameters(args.paramfile)
