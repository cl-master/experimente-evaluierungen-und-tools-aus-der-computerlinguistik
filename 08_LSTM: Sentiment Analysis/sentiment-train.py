#!/usr/bin/env python3

from torchtext import data
import torch
import torch.nn as nn
import torch.optim as optim
from network import NetworkModel
import argparse

parser = argparse.ArgumentParser(description='Training sentiment classifier...')
parser.add_argument("datafile", help="provide the data file")
parser.add_argument('paramfile', help='provide a parameter file')
parser.add_argument('-e', '--num_epochs', type=int, help='times for iteration', default=10)
parser.add_argument('-m', '--emb_size', type=int, help='embedding size', default=200)
parser.add_argument('-r', '--rnn_size', type=int, help='rnn size for BiLSTM', default=300)
parser.add_argument('-d', '--dropout_rate', type=float, help='dropout_rate', default=0.3)
parser.add_argument('-l', '--learning_rate', type=float, help='learning_rate', default=0.01)
args = parser.parse_args()

# create field for text and label
TEXT = data.Field(sequential=True, tokenize=lambda x:x.split(), lower=True, batch_first=True)
LABEL = data.Field(sequential=False, use_vocab=False, batch_first=True)

DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# split the data set into train, dev and test
train, dev, test = data.TabularDataset.splits(
        path=args.datafile, train='sentiment.train.tsv',
        validation='sentiment.dev.tsv', test='sentiment.test.tsv', format='tsv',
        fields=[('sentiment', LABEL), ('sentence', TEXT)])

# create train and dev iterator with batch size
train_iter, dev_iter = data.BucketIterator.splits(
        (train, dev), batch_sizes=(200, 64), sort_key=lambda x: len(x.sentence),
        sort_within_batch=True, repeat=False, device=DEVICE, shuffle=True)

test_iter = data.Iterator(test, batch_size=128, train=False,
                          sort=False, device=DEVICE)

# build vocabulary
LABEL.build_vocab(train)
TEXT.build_vocab(train, min_freq=2)
num_label = len(LABEL.vocab)
num_vocab = len(TEXT.vocab)

# initialize model
model = NetworkModel(num_vocab, num_label, args.emb_size, args.rnn_size, args.dropout_rate)
model = model.to(DEVICE)

loss_function = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=args.learning_rate)
accuracy = 0

# start training
for epoch in range(args.num_epochs):
        print('{} iteration start...'.format(epoch+1))
        for batch in train_iter:
                data = batch.sentence
                tags = batch.sentiment

                model.zero_grad()

                out = model(data)

                loss = loss_function(out, tags)

                loss.backward()
                optimizer.step()

        with torch.no_grad():
                correct_num = 0
                all_num = 0
                for dev_batch in test_iter:
                        data = dev_batch.sentence
                        tags = dev_batch.sentiment

                        score = model(data)
                        pred_tags = torch.argmax(score, dim=1)

                        all_num += tags.size(0)
                        correct_num += torch.eq(tags, pred_tags).sum().item()

                accuracy_dev = correct_num / all_num
                print(epoch + 1, ' iteration : accuracy ', accuracy_dev)
                if accuracy_dev > accuracy:
                        accuracy = accuracy_dev
                        torch.save(model, args.paramfile)
                        print('model is saved!')
