#!/usr/bin/env python3

import torch
import torch.nn as nn


class NetworkModel(nn.Module):
    def __init__(self, num_vocab, num_label, emb_size, lstm_size, dropout_rate):
        super().__init__()
        self.emb = nn.Embedding(num_vocab, emb_size)
        self.dropout = nn.Dropout(p=dropout_rate)
        self.lstm = nn.LSTM(emb_size, lstm_size, batch_first=True, bidirectional=True)
        self.linear = nn.Linear(lstm_size*2, num_label)

    # sentence.shape = (batch_size, sentence_length)
    def forward(self, sentence):
        embeds = self.emb(sentence)  # (batch_size, sentence_length, emb_size)
        embeds = self.dropout(embeds)
        out, _ = self.lstm(embeds)  # (batch_size, sentence_length, rnn_size*2)
        out = torch.max(self.dropout(out), dim=1).values  # (batch_size, rnn_size*2)
        out = self.linear(self.dropout(out))  # (batch_size, number_label)
        return out


