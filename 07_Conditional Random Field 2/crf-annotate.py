#!/usr/bin/env python3

import sys
import pickle

paramfile = sys.argv[1]
textfile = sys.argv[2]


class Tagger:
    def __init__(self, paramfile, textfile):
        with open(paramfile, 'rb') as file:
            params = pickle.load(file)
        self.weights, self.tagset, self.fix_len = params['weights'], params['tagset'], params['fix_len']
        self.text = self.read_data(textfile)
        self.predict_tag()

    def read_data(self, file):
        words, sentences = [], []
        with open(file, 'r') as file:
            for line in file:
                if line != '\n':
                    words.append(line)
                else:
                    sentences.append(words)
                    words = []
            if words:
                sentences.append(words)
        return sentences

    def extract_lex_feature(self, tag, words, pos):
        lex_features = []

        # word+tag -> WT+w+t
        wt = "WT+" + words[pos] + "+" + tag
        lex_features.append(wt)

        # prefix+tag -> PT+prefix+tag,  suffix+tag -> ST+suffix+tag,
        for i in range(2, min(self.fix_len, len(words[pos]))+1):
            pt = "PT+" + words[pos][:i] + "+" + tag
            st = "ST+" + words[pos][-i:] + "+" + tag
            lex_features.extend((pt, st))

        # tag+previous word -> TW2+tag+word
        tw2 = "TW2+" + tag + "+" + words[pos - 1]  # TW2+t+w-1
        lex_features.append(tw2)

        return lex_features

    def extract_context_feature(self, pretag, tag, words, pos):
        context_features = []
        tt = "TT+" + pretag + "+" + tag
        ttw = "TTW+" + pretag + "+" + tag + "+" + words[pos]
        ttw2 = "TTW2+" + pretag + "+" + tag + "+" + words[pos - 1]
        context_features.extend((tt, ttw, ttw2))
        return context_features

    def cal_lex_score(self, tag, words, pos):
        lex_features = self.extract_lex_feature(tag, words, pos)
        return sum(self.weights[feature] for feature in lex_features)

    def cal_context_score(self, pretag, tag, words, pos):
        context_features = self.extract_context_feature(pretag, tag, words, pos)
        return sum(self.weights[feature] for feature in context_features)

    def predict_tag(self):
        tag_list = []
        for words in self.text:
            prev_tags = ['<s>']
            words = [' '] + words
            for pos in range(1, len(words)):
                prev_tag = prev_tags[pos - 1]
                tag_score = dict()
                for tag in self.tagset:
                    lex_score = self.cal_lex_score(tag, words, pos)
                    context_score = self.cal_context_score(prev_tag, tag, words, pos)
                    tag_score[tag] = lex_score + context_score
                predict_tag = max(tag_score.keys(), key=tag_score.get)
                prev_tags.append(predict_tag)
            tag_list.append(prev_tags)

        for words, tags in zip(self.text, tag_list):
            for word, tag in zip(words, tags):
                print(word, tag, sep='\t')
            print('\n')


if __name__ == '__main__':
    tagger = Tagger(paramfile, textfile)














