#!/usr/bin/env python3

import sys
import argparse
import numpy
import pickle
from collections import defaultdict,Counter
import time as tm


class CrfTagger:
    def __init__(self, train_file, dev_file, fix_len, epochs, learning_rate, l1, threshold):
        datalist, self.tagset = self.read_data(train_file)
        self.fix_len = fix_len
        self.l1 = l1
        self.learning_rate = learning_rate
        self.threshold = threshold
        self.weights = defaultdict(float)
        self.regular_time = defaultdict(int)
        self.time = 0
        self.train(datalist, epochs, dev_file)

    def read_data(self, file):
        datalist, words, tags = [], [" "], ["<s>"] # add <s> as start-tag
        tagset = {'<s>','<e>'}
        with open(file) as f:
            for line in f:
                if line == "\n": # add <e> as end-tag
                    words.append(" ")
                    tags.append("<e>")
                    datalist.append((words, tags))
                    words, tags = [" "], ["<s>"]
                else:
                    word, tag = line.strip().split()
                    words.append(word)
                    tags.append(tag)
                    tagset.add(tag)
        return datalist, tagset

    def extract_lex_feature(self, tag, words, pos):
        lex_features = []

        # word+tag -> WT+w+t
        wt = "WT+" + words[pos] + "+" + tag
        lex_features.append(wt)

        # prefix+tag -> PT+prefix+tag,  suffix+tag -> ST+suffix+tag,
        for i in range(2, min(self.fix_len, len(words[pos]))+1):
            pt = "PT+" + words[pos][:i] + "+" + tag
            st = "ST+" + words[pos][-i:] + "+" + tag
            lex_features.extend((pt, st))

        # tag+previous word -> TW2+tag+word
        tw2 = "TW2+" + tag + "+" + words[pos - 1]  # TW2+t+w-1
        lex_features.append(tw2)

        return lex_features

    def extract_context_feature(self, pretag, tag, words, pos):
        context_features = []
        tt = "TT+" + pretag + "+" + tag
        ttw = "TTW+" + pretag + "+" + tag + "+" + words[pos]
        ttw2 = "TTW2+" + pretag + "+" + tag + "+" + words[pos - 1]
        context_features.extend((tt, ttw, ttw2))
        return context_features

    def cal_lex_score(self, tag, words, pos):
        lex_features = self.extract_lex_feature(tag, words, pos)
        for feature in lex_features:
            if self.regular_time[feature] < self.time:
                self.l1_regular(feature)
                self.regular_time[feature] = self.time
        return sum(self.weights[feature] for feature in lex_features)

    def cal_context_score(self, pretag, tag, words, pos):
        context_features = self.extract_context_feature(pretag, tag, words, pos)
        for feature in context_features:
            if self.regular_time[feature] < self.time:
                self.l1_regular(feature)
                self.regular_time[feature] = self.time
        return sum(self.weights[feature] for feature in context_features)

    def logSumExp(self, ns):
        # calculate the logsumexp trick
        max = numpy.max(ns)
        ds = ns - max
        sumOfExp = numpy.exp(ds).sum()
        return max + numpy.log(sumOfExp)

    def cal_forward(self, words):
        # calculate alpha table in log
        forward = [{"<s>": 1}]
        for pos in range(1, len(words)):
            forward.append(defaultdict(float))
            prun_value = max(forward[pos-1].values()) + numpy.log(self.threshold)
            forward[pos-1] = {k: v for k, v in forward[pos-1].items() if v >= prun_value}

            for tag in self.tagset:
                # calculate the lexical score
                lex_score = self.cal_lex_score(tag, words, pos)

                values = [self.cal_context_score(pretag, tag, words, pos) + p
                          for pretag, p in forward[pos - 1].items()]
                forward[pos][tag] = lex_score + self.logSumExp(values)
        return forward

    def cal_backward(self, words, forward):
        # calculate beta table in log
        backward = [defaultdict(float) for x in range(len(words))]
        backward[-1] = {"<e>": 1}

        for pos in range(len(words) - 1, -1, -1):
            for tag in forward[pos-1].keys():
            #for tag in self.tagset:
                values = [self.cal_lex_score(t, words, pos) +
                      self.cal_context_score(tag, t, words, pos) + p
                      for t, p in backward[pos].items()]
                backward[pos - 1][tag] = self.logSumExp(values)
        return backward

    def cal_expected_freqs(self, words):
        # calculate the expected frequency
        expected_freqs = defaultdict(float)
        forward = self.cal_forward(words)
        backward = self.cal_backward(words,forward)

        for pos in range(1, len(words)):
            #for tag in self.tagset:
            for tag in forward[pos].keys():
                gamma_t = numpy.exp(forward[pos][tag] + backward[pos][tag] - forward[-1]["<e>"])

                for lex_feature in self.extract_lex_feature(tag, words, pos):
                    expected_freqs[lex_feature] += gamma_t
                    lex_score = self.cal_lex_score(tag, words, pos)

                    for pretag in forward[pos - 1]:
                        gamma_tt = numpy.exp(
                            forward[pos-1][pretag] + lex_score + self.cal_context_score(pretag, tag, words, pos)
                            + backward[pos][tag] - forward[-1]["<e>"])

                        for con_feature in self.extract_context_feature(pretag, tag, words, pos):
                            expected_freqs[con_feature] += gamma_tt
        return expected_freqs

    def cal_observed_freqs(self, words, tags):
        # calculate the observed frequency
        observed_freqs = Counter()
        for pos in range(1, len(words)):
            observed_freqs.update(self.extract_lex_feature(tags[pos], words, pos))
            observed_freqs.update(self.extract_context_feature(tags[pos - 1],
                                                       tags[pos], words, pos))
        return dict(observed_freqs)

    def l1_regular(self, feature):
        if self.weights[feature] > 0:
            self.weights[feature] -= self.learning_rate * self.l1
        elif self.weights[feature] < 0:
            self.weights[feature] += self.learning_rate * self.l1

    def train_on_sent(self, words, tags):
        # train on one sentence
        observed_freqs = self.cal_observed_freqs(words, tags)
        expected_freqs = self.cal_expected_freqs(words)
        gradient = defaultdict(float)
        # update weight
        for feature, observed_freq in observed_freqs.items():
            gradient[feature] = observed_freq - expected_freqs[feature]
            self.weights[feature] += gradient[feature] * self.learning_rate

    def train(self, datalist, epochs, devfile):
        best_accuracy = 0
        # train on trainingset
        print('start training...')
        for i in range(epochs):
            start = tm.time()
            print('{} iteration start...'.format(i+1))
            for words, tags in datalist:
                self.time += 1
                self.train_on_sent(words, tags)
            for feature, time in self.regular_time.items():
                if time < self.time:
                    for _ in range(self.time - time):
                        self.l1_regular(feature)
            print('{} iteration finished'.format(i+1))
            print('{} seconds used......'.format(tm.time()-start))
            current_acc = self.evaluation_on_dev(dev_file)
            print('Accuracy: {}'.format(current_acc))
            if current_acc > best_accuracy:
                best_accuracy = current_acc
                self.save_paras('paramfile')

    def evaluation_on_dev(self, file):
        dev_file = self.read_data(file)[0]
        total, correct = 0, 0
        for words, tags in dev_file:
            prev_tags = ['<s>']
            for pos in range(1, len(words)):
                total +=1
                prev_tag = prev_tags[pos-1]
                tag_score = dict()
                for tag in self.tagset:
                    lex_score = self.cal_lex_score(tag,words,pos)
                    context_score = self.cal_context_score(prev_tag,tag,words,pos)
                    tag_score[tag] = lex_score + context_score
                predict_tag = max(tag_score.keys(), key=tag_score.get)
                correct +=1 if prev_tag == tags[pos] else 0
                prev_tags.append(predict_tag)
        return correct/total

    def save_paras(self, parafile):
        # save the parameters
        paras = {"weights": self.weights,
                 "tagset": self.tagset,
                 "fix_len": self.fix_len}
        with open(parafile, 'wb') as f:
            pickle.dump(paras, f)


if __name__ == '__main__':
    #parser = argparse.ArgumentParser()
    #parser.add_argument("trainfile", type=str)
    #parser.add_argument("paramfile", type=str)
    #parser.add_argument("--epochs", type=int, default=5)
    #parser.add_argument("--fix_length", type=int, default=5)
    #parser.add_argument("--learning_rate", type=float, default=0.09999)

    #args = parser.parse_args()

    train_file = './Tiger/train.txt'
    dev_file = './Tiger/develop.txt'
    paramfile = 'paramfile'

    crf = CrfTagger(train_file, dev_file, 5, 5, 0.01,0.01,0.001)
    #crf.save_paras(paramfile)

