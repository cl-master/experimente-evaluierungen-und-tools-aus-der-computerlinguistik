#!/usr/bin/env python3

import sys
import pickle
import math

paramfile = sys.argv[1]
inputfile = sys.argv[2]
#paramfile = 'paramfile'
#inputfile = './test/spam/1219.2004-10-25.BG.spam.txt'

# read parameter from file
with open(paramfile, 'rb') as file:
    map_table = pickle.load(file)

priori_spam = map_table['priori_spam']
priori_ham = map_table['priori_ham']
spam_word_prob = map_table['spam_word_prob']
ham_word_prob = map_table['ham_word_prob']


# compare the probability and return the label of test file
def predict_label(tokens):
    spam_prob,ham_prob = 0,0
    for word in tokens:
        spam_prob += math.log(spam_word_prob.get(word,1))
        ham_prob += math.log(ham_word_prob.get(word,1))
    spam_prob += math.log(priori_spam)
    ham_prob += math.log(priori_ham)
    label = 'spam' if spam_prob > ham_prob else 'ham'
    return label


with open(inputfile, 'r', encoding='ISO-8859-1') as file:
    content = file.read()
tokens = content.split()

label = predict_label(tokens)

print('The class of {} is: {}'.format(inputfile, label))
