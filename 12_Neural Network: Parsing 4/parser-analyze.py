#!/usr/bin/env python3

from Data import Data, NoConstLabelID
import torch
import sys

paramfile = sys.argv[1]
test_file = sys.argv[2]
#paramfile = 'paramfile'
#anotate_file = 'annotate.txt'

data = Data(paramfile+".io")  # read the symbol mapping tables
parser = torch.load(paramfile+".rnn")  # read the model


def parse(sentence):
    suffix, prefix = data.words2charIDvec(sentence)
    scores = parser(torch.tensor(suffix, dtype=torch.long), torch.tensor(prefix, dtype=torch.long))
    labels_id = torch.argmax(scores, dim=1)
    constituent = []
    n = len(sentence)
    for l in range(1, n+1):
        for i in range(0, n-l+1):
            k = i+l
            label = labels_id[int((n+n-l+2)*(l-1)/2)+i]
            if label != NoConstLabelID:
                label = data.ID2label[label]
                constituent.append((label, i, k))
    constituent = sorted(constituent, key=lambda x: x[2]-x[1], reverse=True)
    return constituent


def build_parse(words, constituent):
    constituent1 = []
    # split the tags combined before
    for tag, start, end in constituent:
        if ' ' in tag:
            tags = tag.split(' ')
            for t in tags:
                constituent1.append((t,start,end))
        else:
            constituent1.append((tag,start,end))
    tree = ''
    for pos in range(len(words)):
        for tag, start, _ in constituent1:
            if start == pos:
                tree += '(' + tag
        tree += ' '+words[pos]
        for _, _, end in constituent1:
            if end == pos+1:
                tree += ')'
    return tree


sentences = data.sentences(test_file)
for s in sentences:
    constituent = parse(s)
    tree = build_parse(s, constituent)
    print(s,constituent)
    print(tree, '\n')
