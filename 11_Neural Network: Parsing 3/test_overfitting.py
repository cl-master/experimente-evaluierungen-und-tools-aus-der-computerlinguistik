import torch
import torch.nn as nn
from Data import Data, NoConstLabelID


parser = torch.load("paramfile_cpu.rnn")
data = Data('./data/train.txt', './data/dev.txt')
print('prefix and suffix')
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def rewrite_labeltensor(labeltensor, words, constituents):
    num_span = (len(words)+1)*len(words)/2  # total number of spans
    index_list = list(range(int(num_span)))  # list all the index of spans
    group_size = range(len(words), 0, -1)  # split the index into several group with their span length
    chunks = []  # [[span index with length 1],[span index with length 2],......]
    count = 0
    for size in group_size:
        chunks.append([index_list[i + count] for i in range(size)])
        count += size
    # change the value of the span_index with its true label
    for label, start_index, end_index in constituents:
        labeltensor[chunks[end_index-start_index-1][start_index]] = data.labelID(label)
    return labeltensor


parser.train(False)

num_error, num_correct = 0, 0
with torch.no_grad():
    for words, constituents in data.dev_parses:
        parser.zero_grad()

        prefix, suffix = data.words2charIDvec(words)

        label_scores = parser(torch.tensor(prefix, dtype=torch.long,device=DEVICE), torch.tensor(suffix, dtype=torch.long,device=DEVICE))
        label_predict = torch.argmax(label_scores, dim=1)

        label_tensor = torch.empty(label_scores.size(0),dtype=torch.long,device=DEVICE).fill_(NoConstLabelID)
        label_target = rewrite_labeltensor(label_tensor, words, constituents)

        num_error += (label_predict != label_target).sum().item()
        num_correct += (label_predict == label_target).sum().item()

    print("Number of error: {}, Num of correct: {}".format(num_error,num_correct))
    print("accuracy: {}".format(num_correct/(num_error+num_correct)))
    torch.save(parser.cpu(), "paramfile_cpu.rnn")

data.store_parameters("paramfile.io")
