#!/usr/bin/env python3

import torch
import torch.nn as nn
import random
import argparse
import math
from Data import Data,NoConstLabelID
from network import Parser
import time

parser = argparse.ArgumentParser()
parser.add_argument("path_train", type=str)
parser.add_argument("path_dev", type=str)
parser.add_argument("paramfile", type=str)
parser.add_argument('-n',"--num_epochs", type=int, default=20)
parser.add_argument('-e',"--char_emb_size", type=int, default=100)
parser.add_argument('-c',"--char_lstm_size", type=int, default=100)
parser.add_argument('-l',"--num_layers", type=int, default=2)
parser.add_argument('-w',"--word_lstm_size", type=int, default=200)
parser.add_argument('-hs',"--hidden_size", type=int, default=100)
parser.add_argument('-d',"--dropout_rate", type=float, default=0.5)
parser.add_argument('-lr',"--learning_rate", type=float, default=0.001)

args = parser.parse_args()

data = Data(args.path_train, args.path_dev)
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

parser = Parser(charVocabSize=data.num_char_types(), charEmbSize=args.char_emb_size, charLSTMSize=args.char_lstm_size,
                numLayers=args.num_layers, wordLSTMSize= args.word_lstm_size, hiddenSize=args.hidden_size,
                numCategories=data.num_label_types(), dropout=args.dropout_rate, device=DEVICE)

parser = parser.to(DEVICE)

loss_function = nn.CrossEntropyLoss()
optimizer = torch.optim.AdamW(parser.parameters(), lr=args.learning_rate)


def rewrite_labeltensor(labeltensor, words, constituents):
    num_span = (len(words)+1)*len(words)/2  # total number of spans
    index_list = list(range(int(num_span)))  # list all the index of spans
    group_size = range(len(words), 0, -1)  # split the index into several group with their span length
    chunks = []  # [[span index with length 1],[span index with length 2],......]
    count = 0
    for size in group_size:
        chunks.append([index_list[i + count] for i in range(size)])
        count += size
    # change the value of the span_index with its true label
    for label, start_index, end_index in constituents:
        labeltensor[chunks[end_index-start_index-1][start_index]] = data.labelID(label)
    return labeltensor


num_error_min = math.inf

for epoch in range(args.num_epochs):
    start = time.time()
    print('training {} epoch...'.format(epoch+1))

    random.shuffle(data.train_parses)

    parser.train(True)
    for words, constituents in data.train_parses:

        parser.zero_grad()

        suffix, prefix = data.words2charIDvec(words)

        label_scores = parser(torch.tensor(suffix, dtype=torch.long, device=DEVICE), torch.tensor(prefix, dtype=torch.long, device=DEVICE))

        label_tensor = torch.empty(label_scores.size(0), dtype=torch.long, device=DEVICE).fill_(NoConstLabelID)
        label_target = rewrite_labeltensor(label_tensor, words, constituents)

        loss = loss_function(label_scores, label_target)
        loss.backward()
        optimizer.step()

    parser.train(False)

    num_error, num_correct = 0, 0
    with torch.no_grad():
        for words, constituents in data.dev_parses:
            parser.zero_grad()

            suffix, prefix = data.words2charIDvec(words)

            label_scores = parser(torch.tensor(suffix, dtype=torch.long, device=DEVICE), torch.tensor(prefix, dtype=torch.long,device=DEVICE))
            label_predict = torch.argmax(label_scores, dim=1)

            label_tensor = torch.empty(label_scores.size(0), dtype=torch.long, device=DEVICE).fill_(NoConstLabelID)
            label_target = rewrite_labeltensor(label_tensor, words, constituents)

            num_error += (label_predict != label_target).sum().item()
            num_correct += (label_predict == label_target).sum().item()

        print("Number of error: {}, Num of correct: {}".format(num_error, num_correct))
        print("{} seconds used...".format(time.time()-start))
        if num_error < num_error_min:
            num_error_min = num_error
            print("Got minimum Error number {}, saving model...".format(num_error_min))
            torch.save(parser, args.paramfile + ".rnn")

data.store_parameters(args.paramfile + ".io")


