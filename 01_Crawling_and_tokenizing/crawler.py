#!/usr/bin/env python3

import sys
import re

#filename = sys.argv[-2]
#filename2 = sys.argv[-1]
filename = 'music'
filename2 = 'abbreviations'

# open html file and abbreviation file
with open(filename,'r') as file:
    html = file.read()
with open(filename2,'r') as file2:
    abbrreviation = file2.readlines()

# delete '\n' on the end of each line and save the word list
abbrreviation = sorted([a[:-1] for a in abbrreviation],reverse=True)

# clean the html entities
# TODO: better way to clean html entities?
rules = {r'\\"': '"', r'&#x27;': "'", r'&amp;': "&", r'&#39;': "'",
         r'&ldquo;': '"', r'&rdquo;': '"',r'&rsquo;': "'", r'&quot;': ':',
         r'<em>':' ', r'</em>':' ', '<a href=.+?>':' ',
         '</a>':' ', '&nbsp; ':' ', r'\\\\"':'"'}
for k, v in rules.items():
    regex = re.compile(k)
    html = regex.sub(v, html)

# make the match patterns for different html structures
title = re.compile(r'<title data-rh="true">(.+)</title>')
paragraph_pattern = re.compile(r'"paragraph","model":{"text":"(.+?)","blocks|"crosshead","text":"(.+?)"')

text_block = re.compile(r'"attributes\":\[\],\"text\":\"([^<>{}]+)\"}')

description = re.compile(r'"description" content="(.+?)"/>')
title1 = re.compile(r'<h2.*?>([^<>{}]+)</h2></blockquote>')
title2 = re.compile(r'<p><strong>([^<>{}]+)</strong></p>')
paragraph = re.compile(r'<p.*?>([^<>{}]+)</p>')


title = re.findall(title, html)
if not title:
    title = re.findall(r'<title>(.+)</title>',html)
    if not title:
        title = [' ']
clean_text = []

# if the html from BBC news, we can use tag "paragraph"
if re.findall(paragraph_pattern, html):
    clean_text += title
    paragraphs = re.findall(paragraph_pattern,html)
    for para, crosshead in paragraphs:
        if para:
            clean_text.append(para)
        elif crosshead:
            clean_text.append(crosshead)
# if the html from BBC sport
elif re.findall(text_block, html):
    clean_text +=title
    paragraphs = re.findall(text_block, html)
    clean_text += paragraphs
# if the html from other categories, we have to deal with each tag separately
# match the text body and note the index
else:
    text_and_index = {}
    for pattern in (title1,title2, description, paragraph):
        for para in re.finditer(pattern,html):
            text_and_index[para.group(1)] = para.span()[-1]
    clean_text = [t for t,i in sorted(text_and_index.items(),key=lambda x:x[1])]
    clean_text.insert(0, title[0])

# find all abbreviation firstly and substitute them with placeholder+id
# TODO: better way to find abbreviations?
abbreviation_dict = {i: w for i, w in enumerate(abbrreviation)}
text = ' '.join(clean_text)

for i, w in abbreviation_dict.items():
    w = w.replace('.', '\.')
    regex = re.compile('( |^)'+w+' ')
    text = regex.sub(' placeholder'+str(i)+' ', text)

# tokenize the text with re
token=re.compile(r'(\w+|[^\s]\W?)')
tokens=re.findall(token, text)
tokenized_text = ' '.join(tokens)

# segment text into sentences
# TODO: better way to recognize the title sentence and abbreviation not in list?
sent = re.compile(r'[\w\d][^\.?!]*[\.{1,6}?!]')
sents = re.findall(sent, tokenized_text)

# substitute the placeholder with original abbreviations and print sentence
placeholder = re.compile(r'placeholder\d+')
for s in sents:
    if re.findall(placeholder,s):
        for x in re.findall(placeholder,s):
            s = s.replace(x, abbreviation_dict[int(x[11:])])
        print(s)
    else:
        print(s)
