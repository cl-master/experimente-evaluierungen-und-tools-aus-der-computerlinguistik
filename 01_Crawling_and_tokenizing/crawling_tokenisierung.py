import sys
import re
import html

# The module requires 2 arguments：argument 1 is path of html article, argument 2 is path of abbreviation list
html_file = sys.argv[1]
abbr_file = sys.argv[2]


"""
Read html file and abbreviation
"""
with open(html_file, "r", encoding="utf-8") as f:
    html_raw = f.read()

with open(abbr_file, "r", encoding="utf-8") as f:
    abbr = f.readlines()

# delete '\n' on the end of each line and save abbrevations in a set
abbr = set([a[:-1] for a in abbr])


"""
Creat patterns for matching plain html
"""
title_r = re.compile(r'<title.*>(.+)</title>') # pattern for title of articles
paragraph_r1 = re.compile(r'<p>(.+?)</p>') # pattern for paragraphs of bbc.com (all subjects besides sports)
paragraph_rs = re.compile(r'<p data-reactid.+?>(.+?)</p>') # pattern for paragraphs of bbc.com/sports
clean_rt = re.compile(r'<.*?>') # pattern for format tags with text in the middle, like <em>, <ref...>
clean_rs = re.compile(r'<style.*?>.+?</style>') # pattern for <style>-tag, with no text in the middle


"""
Find clean plain html text
"""
title = re.findall(title_r, html_raw)[0] + "."  # match the title of article # mark sentence end with "."

body = re.findall(paragraph_r1, html_raw)  # match paragraphs of bbc.com (all subjects besides sports)
if not body:  # match paragraphs of bbc.com/sports
    body = re.findall(paragraph_rs, html_raw)
body = " ".join(body)
body = re.sub(clean_rs, "", body) # clean <style>-tag, with no text in the middle
body = re.sub(clean_rt, "", body) # clean format tags with text in the middle, like <em>, <ref...>

html_clean = title + " " + body # combine title and body

html_clean = html.unescape(html_clean) # clean html entities


"""
Tokenize into words
"""
html_token = re.findall(r'\d+[\.,]\d+'  # pattern for digits like 4,000 oder 3.25
                        r'|(?:\w+\.)+\w+\.'  # pattern for abbreviations with more than 1 dot (also unknown abbr not in the list)
                        r'|\w+(?:[-]\w+)*'  # pattern for words with non-breaking hyphen like Covid-19
                        r'|"'  # pattern for " "
                        r'|[-.(]+' # pattern for left parenthesis, ellipsis and double-hyphen
                        r'|\S\w*', html_clean) # pattern for others


"""
Tokenize into sentences and handle the abbreviation in the list
"""
sentence_endmark = ["!", "?", ".", "..."]

sents = []  # list of all sentences
sent = []  # list of single sentence
skip_next = False

# interate all the tokens in the list, finding whole sentence and abbreviations
for i in range(len(html_token)):
    # skip the dot when an abbreviation was found before
    if skip_next:
        skip_next = False
        continue
    # handle the situation: the dot build with the former token together the abbreviation in the list
    if i < len(html_token) - 1 and html_token[i+1] == "." and html_token[i] + html_token[i+1] in abbr:
        sent.append(html_token[i] + html_token[i+1])
        skip_next = True
    # handle the situation with sentence endmark, build the sentence
    elif html_token[i] in sentence_endmark:
        sent.append(html_token[i])
        sent_s = " ".join(sent)
        sents.append(sent_s)
        sent = []
    # handle the situation without sentence endmark and dot of abbreviation
    else:
        sent.append(html_token[i])


"""
Output
"""
print(*sents, sep="\n")

