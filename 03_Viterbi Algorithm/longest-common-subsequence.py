#!/usr/bin/env python3

import sys

# Read the input strings
s = sys.argv[1]
t = sys.argv[2]

m = len(s)
n = len(t)

# build the Matrix l (n*m) with all empty strings in it
l = [["" for x in range(m+1)] for y in range(n+1)]

for i in range(1, n+1):
    for j in range(1, m+1):
        # calculate the viterbi values
        viterbi_l = [len(l[i-1][j]), len(l[i][j-1]), (len(l[i-1][j-1]) + (1 if t[i-1] == s[j-1] else 0))]
        # calculate the index of the max viterbi value
        viterbi_max = viterbi_l.index(max(viterbi_l))

        # calculate the string (longest common subsequence) in l[i,j]
        if viterbi_max == 0:
            l[i][j] = l[i-1][j]
        elif viterbi_max == 1:
            l[i][j] = l[i][j-1]
        elif viterbi_max == 2:
            l[i][j] = l[i-1][j-1] + t[i-1]

# Output
print("The longest common subsequence is", l[n][m])

