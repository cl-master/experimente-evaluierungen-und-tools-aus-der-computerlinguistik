$verb-pres-infl$ = <verb-pres> (\
    {<inf>}:{en} |\
    {<1s>}:{<ST>} |\
    {<2s>}:{<ST>t} |\
    {<3s>}:{<ST>t} |\
    {<1pl>}:{en} |\
    {<2pl>}:{en} |\
    {<3pl>}:{en})

$verb-past-infl$ = <verb-past> (\
    {<1s>}:{<ST>de} |\
    {<2s>}:{<ST>de} |\
    {<3s>}:{<ST>de} |\
    {<1pl>}:{<ST>den} |\
    {<2pl>}:{<ST>den} |\
    {<3pl>}:{<ST>den})

$verb-participle-infl-1$ = {}:{ge}
$verb-participle-infl-2$ = <verb-part> {}:{<ST>d}

$noun-inf$ = <noun> ({<sg>}:{}|{<pl>}:{en})


$morph$ = "verb.lex" ($verb-pres-infl$|$verb-past-infl$) |\
    $verb-participle-infl-1$ "verb.lex" $verb-participle-infl-2$ |\
    "noun.lex" $noun-inf$

#vowel# = aeiou 
#cons# = bcdfghjklmnpqrstvwxz 
#t-verb# = thfcksp
$vowel$ = [#vowel#] 
$cons$ = [#cons#]
$t-verb$ = [#t-verb#]
#=c# = #cons#
#=v# = #vowel#

ALPHABET = [A-Za-z'] <verb-pres> <verb-past> <verb-part> <ST> <noun>

% <verb> duplicate vowel
$duplicate-v$ = {[#=v#]}:{[#=v#][#=v#]} ^-> ($cons$__($cons$|<>|$vowel$)[<verb-pres><verb-past><verb-part>]<ST>)

% <verb> delete consonant if there're double consonant on the end
$delete-c$ = {[#=c#][#=c#]}:{[#=c#]} ^-> (__[<verb-pres><verb-past><verb-part>]<ST>)

% <verb> substitute v,z with f,s if it is on the end
$substitute-vz$ = (v:f|z:s) ^-> (__[<verb-pres><verb-past><verb-part>]<ST>)

% <verb> t-verb has suffix te/ten
$to-t-verb$ = (d:t) ^-> ($t-verb$[<verb-past><verb-part>]<ST>__)

% <noun> keeping vowels short/long
$duplicate-c$ = {[#=c#]}:{[#=c#][#=c#]} ^-> ($cons$ $vowel$__<noun>en)

% <noun> no open double vowel
$delete-double-v$ = {[#=v#][#=v#]}:{[#=v#]} ^-> ($cons$__$cons$ <noun>en)

% <noun> replace hard f/s by soft v/z
$replace-fs$ = (s:z|f:v) ^-> ($vowel$__<noun>en)

% <noun> plural ending with 's
$plural-as$ = {en}:{'s} ^-> ([aiouy]<noun>__)

% <noun> plural ending with s
$plural-s-1$ = {en}:{s} ^-> ((.?$vowel$+.?)+ $cons$ (e|ee|el|em|er|erd|aar|aard|um|eur) <noun>__)
$plural-s-2$ = {en}:{s} ^-> ((.?$vowel$+.?)+ foon <noun>__)

$delete-pos$ = ([<verb-pres><verb-past><ST><verb-part><noun>]:<>) ^-> ()

$verb$ = $duplicate-v$ || $delete-c$ || $to-t-verb$ || $substitute-vz$
$noun$ = $plural-as$ || $plural-s-1$ || $plural-s-2$ || $duplicate-c$ || $delete-double-v$ || $replace-fs$

$morph$ || $verb$ || $noun$ || $delete-pos$
