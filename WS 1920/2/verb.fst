ALPHABET = [A-ZÄÖÜa-zaöüß]

$verb_reg$ = "verb-reg.lex"


$verb_pres_reg$={<pres><1><sg>}:{e}|\
{<pres><2><sg>}:{st}|\
{<pres><3><sg>}:{t}|\
{<pres><1><pl>}:{en}|\
{<pres><2><pl>}:{t}|\
{<pres><3><pl>}:{en}|\
{<pres><2><sgpl>}:{en}

$verb_past_reg$={<past><1><sg>}:{te}|\
{<past><2><sg>}:{test}|\
{<past><3><sg>}:{te}|\
{<past><1><pl>}:{ten}|\
{<past><2><pl>}:{tet}|\
{<past><3><pl>}:{ten}|\
{<past><2><sgpl>}:{ten}

$verb_pres_konj$ = $verb_reg$ $verb_pres_reg$
$verb_past_konj$ = $verb_reg$ $verb_past_reg$

$verb_past_konj$ | $verb_pres_konj$




