from nltk import word_tokenize
from nltk import FreqDist
from nltk import ConditionalFreqDist
from nltk.corpus import stopwords
import os, math
from collections import defaultdict
import json
import string
import random
import numpy

#Xiyue Cui 10995601
#Zheng Hua 11011680
#Shuzhou Yuan 11993161


#reading instances
class Reviews:
    def __init__(self,review_list):
        self.review_list=review_list

    @classmethod #getting all training instances list
    def get_datelist(cls,path):
        pathlist = os.listdir(path)
        datelist = [os.path.abspath(path+p) for p in pathlist]
        return cls.get_rawtext(datelist)

    @classmethod #reading instances from paths and getting labels, deleting stopwords and punctuation
    def get_rawtext(cls,path_list):
        reviews=[]
        for path in path_list:
            with open (path,'r',encoding = "ISO-8859-1") as f:
                content=f.read()
                features=word_tokenize(content)
                features1=[word for word in features if word not in stopwords.words('english')]
                features2=[word for word in features1 if word not in string.punctuation]
                feature_count=FreqDist(features2)
                feature_count1={k:v for k,v in sorted(feature_count.items(),key=lambda x:-x[1])}
                label='neg' if '/neg/' in path else 'pos'
                reviews.append((label,feature_count1,path))
        return cls(reviews)


class Log_linear_modell:
    def __init__(self,feature_dict,weight_dict,categories):
        self.fearure_dict=feature_dict
        self.weight_dict=weight_dict
        self.categories=categories

    #initializing from reviews
    @classmethod
    def from_reviews(cls,reviews):
        featue_dict=defaultdict(int)
        weight_dict=defaultdict(int)
        categories = set([x[0] for x in reviews.review_list])
        for rewiew in reviews.review_list:
            for feature,count in rewiew[1].items():
                featue_dict[(feature,rewiew[0])]=1
                for label in categories:
                    if label!=rewiew[0]:
                        featue_dict[(feature,label)]=0
                weight_dict[(feature,rewiew[0])]=0
        return cls(featue_dict,weight_dict,categories)


    def score_each_class(self,feature_counts):
        scores=defaultdict(int)
        categories=self.categories
        for cate in categories:
            scores[cate]=sum([self.fearure_dict[(feature,cate)]*self.weight_dict[(feature,cate)]*count
                              for feature,count in feature_counts.items()])
        return scores

    def normalise_of_scores(self,scores):
        return {cate:numpy.exp(score)/sum(numpy.exp(score) for score in scores.values()) for cate,score in scores.items()}

    def predict_category(self,feature_count):
        return sorted(self.normalise_of_scores(self.score_each_class(feature_count)).items(),key=lambda x:-x[1])[0][0]

    def derivative_of_ll(self,word,label):
        E=self.normalise_of_scores(self.score_each_class({word:1}))
        return self.fearure_dict[(word,label)]-sum([e*self.fearure_dict[(word,c)] for c,e in E.items() if c!=label])

    def gradient(self,word,label,learing_rate):
        self.weight_dict[(word,label)]+=learing_rate*(self.derivative_of_ll(word,label)-0.21*self.weight_dict[(word,label)])
        print('{} {}: '.format(word,label)+str(self.weight_dict[(word,label)]))

    def training(self,reviews):
        labels=set([label[1] for label in self.fearure_dict.keys()])
        for cate in labels:
            for review in reviews.review_list:
                print('review : {}'.format(review[2]))
                for word in review[1].keys():
                    self.gradient(word,cate,0.6)

    def accuracy(self,reviews):
        correct_num=0
        num=0
        for review in reviews.review_list:
            num+=1
            pred_label=self.predict_category(review[1])
            if review[0]==pred_label:
                correct_num+=1
                print('True label: '+review[0]+'\npred label: '+pred_label)
            else:
                print('True label: '+review[0]+'\npred label: '+pred_label)
        print(correct_num)
        print(num)
        return correct_num/num



if __name__ == '__main__':

    def load_reviews():
        path = os.path.abspath('.')
        # pos_ids = os.listdir(path + "/txt_sentoken/pos")
        # neg_ids = os.listdir(path + "/txt_sentoken/neg")
        pos_reviews = Reviews.get_datelist(path + "/txt_sentoken/pos/")
        neg_reviews = Reviews.get_datelist(path + "/txt_sentoken/neg/")

        test_pos = pos_reviews.review_list[:]
        test_neg = neg_reviews.review_list[:]
        test_collection = test_pos + test_neg
        #dev_pos = pos_reviews.review_list[50:100]
        #dev_neg = neg_reviews.review_list[50:100]
        #dev_collection = dev_pos + dev_neg
        training_pos = pos_reviews.review_list[:]
        training_neg = neg_reviews.review_list[:]
        training_collection = training_pos + training_neg

        random.shuffle(training_collection)
        #random.shuffle(dev_collection)
        random.shuffle(test_collection)

        training_set = Reviews(training_collection)
        #dev_set = Reviews(dev_collection)
        test_set = Reviews(test_collection)

        return training_set, test_set


    print("Preparing data...")
    training_set, test_set = load_reviews()

    print('Training classifier...')
    klassifikator = Log_linear_modell.from_reviews(training_set)
    klassifikator.training(training_set)

    print('The accuracy of test data is:')
    print(klassifikator.accuracy(test_set))

'''
Liebe Studenten,

hier sind meine Kommentare zu Ihrer Lösung für Aufgabe 4.

Viele Grüße,
Helmut Schmid




Die Regularisierung fehlt, etwas kompliziert, kleinere Fehler

Diese beiden Funktionen hätte ich nicht zu einer Klasse
zusammengefasst, sondern einfach als normale Funktionen definiert:
    def get_datelist(cls,path):
    def get_rawtext(cls,path_list):

nltk ist nicht generell erlaubt!!!
   from nltk import word_tokenize
   from nltk import FreqDist
   from nltk import ConditionalFreqDist
   from nltk.corpus import stopwords

Das Sortieren ist hier überflüssig:
         feature_count1 = {k:v for k,v in
sorted(feature_count.items(),key=lambda x:-x[1])}

Shuffling ist bei den Testdaten überflüssig:
        random.shuffle(test_collection)

Das sollten Sie vor jeder Epoche machen:
        random.shuffle(training_collection)

statt
    def __init__(self,feature_dict,weight_dict,categories):
        self.fearure_dict = feature_dict
        self.weight_dict = weight_dict
        self.categories = categories

    #initializing from reviews
    @classmethod
    def from_reviews(cls,reviews):
        featue_dict = defaultdict(int)
        weight_dict = defaultdict(int)
        categories = set([x[0] for x in reviews.review_list])
        for rewiew in reviews.review_list:
            for feature,count in rewiew[1].items():
                featue_dict[(feature,rewiew[0])] = 1
                for label in categories:
                    if label != rewiew[0]:
                        featue_dict[(feature,label)] = 0
                weight_dict[(feature,rewiew[0])] = 0
        return cls(featue_dict,weight_dict,categories)
    ...
    klassifikator = Log_linear_modell.from_reviews(training_set)
besser
    def __init__(self, reviews):
        self.weight_dict = defaultdict(float)
        self.categories = set(zip(*reviews)[0])
    ...
    klassifikator = Log_linear_modell(training_set)

Hier reicht ein normales Dictionary:
        scores = defaultdict(int)

statt
        categories = self.categories
        for cate in categories:
einfacher
        for cate in self.categories:

statt
            scores[cate] =
sum([self.fearure_dict[(feature,cate)]*self.weight_dict[(feature,cate)]*count
                              for feature, count in feature_counts.items()])
einfacher
            scores[cate] = sum([self.weight_dict[(feature,cate)] * count
                              for feature, count in feature_counts.items()])

statt
        return {cate:numpy.exp(score)/sum(numpy.exp(score) for score in
scores.values()) for cate,score in scores.items()}
effizienter und besser lesbar
        total = sum(numpy.exp(score) for score in scores.values())
        return {cate:numpy.exp(score)/total for cate,score in
scores.items()}
Sie berechnen die Summe mehrmals.

statt
        return
sorted(self.normalise_of_scores(self.score_each_class(feature_count)).items(),key=lambda
x:-x[1])[0][0]
ginge auch
        class_scores = self.score_each_class(feature_count)
        return max(class_scores, key=class_scores.get)

bitte vor Abgabe entfernen:
        print('{} {}:
'.format(word,label)+str(self.weight_dict[(word,label)]))

kompliziert und nicht sehr effizient:
    def derivative_of_ll(self, word, label):
        E = self.normalise_of_scores(self.score_each_class({word:1}))
        return self.fearure_dict[(word,label)] -
sum([e*self.fearure_dict[(word,c)] for c,e in E.items() if c != label])

    def gradient(self,word,label,learing_rate):
        self.weight_dict[(word,label)] +=
learing_rate*(self.derivative_of_ll(word,label)-0.25*self.weight_dict[(word,label)])

    def training(self,reviews):
        labels = set([label[1] for label in self.fearure_dict.keys()])
        for cate in labels:
            for review in reviews.review_list:
                for word in review[1].keys():
                    self.gradient(word,cate,0.1)
besser und korrekter
    def training(self, reviews, num_epochs):
        random.shuffle(reviews)
        for epoch in range(num_epochs):
            for label, feature_count, path in reviews:
            # add observed feature values
                for word, f in feature_count.items():
                    self.weight_dict[(word,label)] += learing_rate * f
            # subtract expected feature values
                class_probs =
self.normalise_of_scores(self.score_each_class(feature_count))
        for c in class_probs.items():
                    for word, f in feature_count.items():
                        self.weight_dict[(word,c)] -= learing_rate * p * f
Sie trainieren nur für eine Epoche und die Schleifen in der Funktion
training scheinen falsch zu sein.

Die Regularisierung fehlt.

Punkte: 7,5

'''



















