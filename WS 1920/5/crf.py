from collections import defaultdict
import math

class ReadingData:
    def __init__(self,word_tag_list,tags_list,words_list):
        self.word_tag_list = word_tag_list
        self.tags_list = tags_list
        self.words_list = words_list

    @classmethod
    def from_filepath(cls,file_path):
        with open(file_path,'r') as f:
            content = f.read().strip()
        sentences = content.split('\n\n')
        word_tag_list = []
        words_list = []
        tags_list = []
        for sentence in sentences:
            sentence_list = []
            words = []
            tags = []
            words.append(' ')
            tags.append('<s>')
            sentence_list.append((' ','<s>'))
            for word_tag in sentence.split('\n'):
                word = word_tag.split('\t')[0]
                tag = word_tag.split('\t')[1]
                sentence_list.append((word,tag))
                words.append(word)
                tags.append(tag)
            words.append(' ')
            tags.append('<s>')
            words_list.append(words)
            tags_list.append(tags)
            sentence_list.append((' ', '<s>'))
            word_tag_list.append(sentence_list)
        return cls(word_tag_list,tags_list,words_list)

class ConditionalRandomField:
    def __init__(self,tag_set, words_set, lexicon_feature, context_feature, weight):
        self.tag_set = tag_set
        self.words_set = words_set
        self.lexicon_feature = lexicon_feature
        self.context_feature = context_feature
        self.weight = weight


    @classmethod
    def from_word_tag_list(cls, data):
        word_tag_list = data.word_tag_list
        tag_set = set([tag for tags in data.tags_list for tag in tags])
        words_set = set([word for words in data.words_list for word in words])
        lexicon_feature = defaultdict(int)
        context_feature = defaultdict(int)
        weight = defaultdict(int)
        for sent in word_tag_list:
            for word, tag in sent:
                lexicon_feature[(word,tag,sent.index((word,tag)))] = 1
                weight[(word,tag,sent.index((word,tag)))] = 0
            for i in range((len(sent))-1):
                context_feature[(sent[i][1],sent[i+1][1],sent[i+1][0],i+1)] = 1
                weight[(sent[i][1],sent[i+1][1],sent[i+1][0],i+1)] = 0
        return cls(tag_set, words_set, lexicon_feature, context_feature, weight)

    def lexikon_features(self, tag, words, i):
        features = []
        for word in words:
            features.append((word,tag,i))
        return features

    def mutiply_weights(self,features):
        return sum([self.weight(feature) for feature in features])

    def lex_score(self,tag, words, i):
        return self.mutiply_weights(self.lexicon_feature(tag,words,i))

    def context_score(self,pre_tag, tag, words, i):
        features=[]
        for word in words:
            features.append((pre_tag,tag,word,i))
        return self.mutiply_weights(features)

    def forward_prob(self,words):
        forward = [{'<s>': 1}]
        for i in range(1,len(words)):
            forward.append(defaultdict(float))
            for tag in self.tag_set:
                lex_score = self.lex_score(tag,words,i)
                for pre_tag, p in forward[i-1].items():
                    forward[i][tag] += p*(lex_score+self.context_score(pre_tag,tag,words,i))
        return forward

    def backward_prob(self,words):
        backward=[]
        for i in range(len(words)-1):
            backward.append(defaultdict(float))
        backward.append({"<s>": 1})
        for i in range(len(words)-1, -1):
            for tag in self.tag_set:
                lex_score = self.lex_score(tag,words,i)
                for pre_tag,p in backward[i].items():
                    backward[i][tag] += p*(lex_score+self.context_score(pre_tag,tag,words,i))
        return backward

    def estimated_freq(self,words):
        forward = self.forward_prob(words)
        backward = self.backward_prob(words)
        for i in range(len(words)):
            for tag in forward[i]:
                p = math.exp(forward[i][tag]+backward[i][tag]-forward[-1]['<s>'])
























if __name__ == '__main__':
    a=ReadingData.from_filepath('../5/Tiger/test.txt')
    b=ConditionalRandomField.from_word_tag_list(a)
    print(b.tag_set)
    print(b.words_set)

