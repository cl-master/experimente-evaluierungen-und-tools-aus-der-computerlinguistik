#Xiyue Cui 10995601
#Zheng Hua 11011680
#Shuzhou Yuan 11993161


import re

formatting_tags = ["/i", "/b", "/small", "/strong", "/em", "/mark", "/del", "/ins", "/sub", "/sup", ""]
def extract_text (html):
    with open (html,'r') as h:
        text=h.read()
    text1 = re.split('<|>', text)
    story_body = get_inner('div class="story-body"', text1)
    header = get_inner('h1 class="story-body__h1"', story_body)
    body = get_inner('div class="story-body__inner"', story_body)
    text_pieces = []
    for n, t in enumerate(body):
        if t == "":
            continue
        if t.split(" ")[0] == "p" or (t[0] == "h" and all([c.isdigit() for c in t[1:].split(" ")[0]])):
            piece = ""
            it = n + 1
            t = body[it]
            while not (t.split(" ")[0] == "/p" or ("/h" in t and all([c.isdigit() for c in t[2:].split(" ")[0]]))):
                if not "/" + t in formatting_tags and not t in formatting_tags:
                    piece += t
                it += 1
                t = body[it]


            if piece == "":
                continue
            while piece[0] == " ":
                piece = piece[1:]
            while piece[-1] == " ":
                piece = piece[:-1]
            if 'a href="' in piece:
                continue
            text_pieces.append(piece)

    return ' '.join(text_pieces)

def get_inner(tag, text_list):
    tag_name = tag.split(" ")[0]
    start = -1
    for n, text in enumerate(text_list):
        if tag in text:
            start = n
            break

    if start == -1:
        print("tag not found")
        return
    end = start
    i = 1
    while i > 0:
        end += 1
        if text_list[end] == "/" + tag_name:
            i -=1
        elif tag_name in text_list[end]:
            i += 1
    return text_list[start + 1:end]


def tokenize(text):
    # regex = r'\"|:|,|\.|!|\?|-|;|\'|\n| '
    words = re.split(" ", text)
    for n, word in enumerate(words):
        if not word == "":
            if "." in word.replace('"', ''):
                # this line: TODO: check if word is abbreviation
                words[n] = "ABBREVIATION." # TODO: (this line) Do something with words[n]
    return ' '.join(words)
    
def text_to_sentences(text):
    sentences = []
    temp = re.split(r"(\.|\?|!)", text)
    for n, s in enumerate(temp[::2]):
        if len(temp) > 2 * n + 1:
            sentences.append(s + temp[2 * n + 1])
        else:
            sentences.append(s)
    return sentences



if __name__ == '__main__':
    file_name = "uk"
    extracted_text = extract_text(file_name)
    unabreviated_text = tokenize(extracted_text)
    sentences = text_to_sentences(unabreviated_text)

    for sentence in sentences:
        print(sentence)

'''
Liebe Studenten,

hier sind meine Kommentare zu Ihrer Übung 1.

Schöne Grüße,
Helmut Schmid

Extraktion: zu kompliziert. besser mit regulären Ausdrücken arbeiten.

Ihr Programm verarbeitet nur eine einzige Datei statt alle im
Verzeichnis www.bbc.com.

Die Eingabedatei
    file_name = "uk"
sollte besser als Argument übergeben werden
    file_name = sys.argv[1]

Ich bekomme eine Fehlermeldung:
  File "Aufgabe1.py", line 46, in get_inner
    for n, text in enumerate(text_list):
  TypeError: 'NoneType' object is not iterable

Wozu machen Sie das?
    text1 = re.split('<|>', text)

n wird hier nicht verwendet, soweit ich sehe:
    for n, t in enumerate(body):

Warum löschen Sie Anführungszeichen, bevor Sie nach Punkten suchen?
            if "." in word.replace('"', ''):

keine Behandlung von Abkürzungen

keine Abtrennung von Satzzeichen

Punkte: 3

'''
        
