import re
import nltk


def gettext (html):
    with open (html,'r') as h:
        text=h.read()
    body=re.compile(r'<p>([^<>]+)</p>')
    text1=re.findall(body,text)
    return ' '.join(text1)


    #rules = [
     #       {r'>\s+': u'>'},  # remove spaces after a tag opens or closes
     #      {r'\s+': u' '},  # replace consecutive spaces
     #       {r'\s*<br\s*/?>\s*': u'\n'},  # newline after a <br>
     #       {r'</(div)\s*>\s*': u'\n'},  # newline after </p> and </div> and <h1/>...
     #       {r'</(p|h\d)\s*>\s*': u'\n\n'},  # newline after </p> and </div> and <h1/>...
     #       {r'<head>.*<\s*(/head|body)[^>]*>': u''},  # remove <head> to </head>
     #       {r'<a\s+href="([^"]+)"[^>]*>.*</a>': r'\1'},  # show links instead of texts
     #       {r'[ \t]*<[^<]*?/?>': u''},  # remove remaining tags
     #       {r'^\s+': u''}  # remove spaces at the beginning
     #   ]
    #for rule in rules:
     #   for (k, v) in rule.items():
      #      regex = re.compile(k)
       #     text = regex.sub(v, text)
        #text = text.rstrip()
    #return text.lower()'''

'''
def tokenize(text):
    tokenized=nltk.word_tokenize(text)
    return tokenized
    '''
def tokenize(text):
    token=re.compile(r'(\w+|\w\.+|[^\s]\W?)',re.I)
    short=re.compile(r'(\w+\.)+')
    sentence=re.compile(r'\w+[^\.]*[\.\?!]')
    tokens=re.findall(token,text)
    sentences=re.findall(sentence,text)
    with open ('abbreviations','r') as a:
        abk=a.readlines()
    for x in re.findall(short,text):
        if x in abk:
            for a in x:
               tokens.remove(a)
            ind = tokens.index(x[0])
            tokens.insert(ind,x)
    for sent in sentences:
        print(sent)
    return tokens



if __name__ == '__main__':
    print(gettext('uk'))
    print(tokenize(gettext('uk')))

        
