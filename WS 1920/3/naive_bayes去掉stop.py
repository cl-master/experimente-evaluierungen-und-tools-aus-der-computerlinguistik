from nltk import word_tokenize
from nltk import FreqDist
from nltk import ConditionalFreqDist
from nltk.corpus import stopwords
import os, math
from collections import defaultdict
import json
import string
import numpy

#Xiyue Cui 10995601
#Zheng Hua 11011680
#Shuzhou Yuan 11993161


#reading instances
class Reviews:
    def __init__(self,review_list):
        self.review_list=review_list

    @classmethod #getting all training instances list
    def get_datelist(cls,path):
        daterootlist = os.listdir(path)
        pathlist = [os.path.abspath(path + p) for p in daterootlist ]
        datelist = [p  for p in pathlist  if os.path.splitext(p)[1] == '.txt']
        return cls.get_rawtext(datelist)


    @classmethod #reading instances from paths and getting labels, deleting stopwords and punctuation
    def get_rawtext(cls,path_list):
        reviews=[]
        for path in path_list:
            with open (path,'r',encoding = "ISO-8859-1") as f:
                content=f.read()
                features=word_tokenize(content)
                features1=[word for word in features if word not in stopwords.words('english')]
                features2=[word for word in features1 if word not in string.punctuation]
                feature_count=FreqDist(features2)

                label='neg' if '/neg/' in path else 'pos'
                reviews.append((label,feature_count,path))
        return cls(reviews)


class Bayes_classifikator:
    def __init__(self,class_to_number,class_word_to_number,vocabulary_dictionary,discount0,prob_unk,vocabulary_len):
        self.class_to_number=class_to_number
        self.class_to_prob={c:n/sum(class_to_number.values())for c,n in self.class_to_number.items()}
        self.class_word_to_number=class_word_to_number
        self.vocabulary_dictionary=vocabulary_dictionary
        self.discount0=discount0
        self.prob_unk=prob_unk
        self.vocabulary_len=vocabulary_len
        self.backoff={category:sum([self.relative_prob(word,category) for category in self.class_word_to_number.conditions()
                                for word in self.class_word_to_number[category].keys()])for category in self.class_word_to_number.conditions()}

    #initializing from reviews
    @classmethod
    def from_reviews(cls,reviews):
        class_word_to_number=ConditionalFreqDist()
        class_to_number=defaultdict(int)
        vocabulary_dictionary = defaultdict(int)
        for cate,feature_count,__ in reviews.review_list:
            class_to_number[cate]+=1
            for feature,num in feature_count.items():
                class_word_to_number[cate][feature]+=num
                vocabulary_dictionary[feature]+=num
        n1=0
        n2=0
        for num in vocabulary_dictionary.values():
            if num==1:
                n1+=1
            elif num==2:
                n2+=1
        discount0=n1/(n1+2*n2) if n1+2*n2!=0 else 0
        vocabulary_len=sum(vocabulary_dictionary.values())
        prob_unk=1-sum([(v-discount0)/vocabulary_len for v in vocabulary_dictionary.values()])
        return cls(class_to_number,class_word_to_number,vocabulary_dictionary,discount0,prob_unk,vocabulary_len)

    #caculating relative probability of particular word in particular category
    def relative_prob(self,word,category):
        n1 = 0
        n2 = 0
        for num in self.class_word_to_number[category].values():
            if num == 1:
                n1 += 1
            elif num == 2:
                n2 += 1
        discount=n1/(n1+2*n2) if n1+2*n2!=0 else 0
        relative_prob=(self.class_word_to_number[category][word]-discount)/sum(self.class_word_to_number[category]
                                                                               .values())
        return max(0, relative_prob)

    #caculating probability of particular word in particular category
    def probability_of_word(self,word,category):
        backoff_factor = 1-self.backoff[category]
        if word in self.vocabulary_dictionary.keys():
            word_prob_in_all = (self.vocabulary_dictionary[word]-self.discount0)/self.vocabulary_len
            word_prob = self.relative_prob(word,category)+backoff_factor*word_prob_in_all
            return numpy.log(word_prob)
        else:
            return numpy.log(self.prob_unk*backoff_factor)

    # caculating probability of text word in particular category
    def probability_of_text(self,feature_counts,category):
        prob = 0
        for word,num in feature_counts.items():
            prob += self.probability_of_word(word,category)*num
            print("probability of {}: {}".format(word, prob))
        return prob+math.log(self.class_to_prob[category])

    # predicting the category of text
    def predict_category(self,feature_counts):
        score_list={category:self.probability_of_text(feature_counts,category) for category in self.class_to_prob.keys()}
        return max(score_list.items(), key=lambda x:-x[1])[0]

    #caculating accuracy of dateset and saving the predicted category in a json document
    def accuracy(self,reviews):
        text_to_class={}
        correct_num=0
        num=len(reviews.review_list)
        for i, (label,feature_count,path) in enumerate(reviews.review_list):
            print("review {}: ".format(i), end="")
            categpry=self.predict_category(feature_count)
            if review[0]==categpry:
                correct_num+=1
                text_to_class[review[2][-15:]]=categpry
                print("success")
            else:
                text_to_class[review[2][-15:]] = categpry
                print("failure")
        with open('text_to_class.json','a') as f:
            json.dump(text_to_class,f)
        return correct_num/num if num!=0 else 0

    #saving the probability of each word in each category in a json document
    def save_training_parameter(self):
        word_to_prob=ConditionalFreqDist()
        with open ('training_parameter.json','a') as t:
            json.dump(self.class_to_prob,t)
            for category in self.class_to_prob.keys():
               word_to_prob[category]={word:self.probability_of_word(word,category) for word in
                                       self.vocabulary_dictionary.keys()}
            json.dump(word_to_prob,t)


















