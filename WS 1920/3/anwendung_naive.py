from naive_bayes去掉stop import Reviews,Bayes_classifikator
import os, sys, random
import json

#Xiyue Cui 10995601
#Zheng Hua 11011680
#Shuzhou Yuan 11993161


def load_reviews():
    path = os.path.abspath('.')
    #pos_ids = os.listdir(path + "/txt_sentoken/pos")
    #neg_ids = os.listdir(path + "/txt_sentoken/neg")
    pos_reviews = Reviews.get_datelist(path + "/txt_sentoken/pos/")
    neg_reviews = Reviews.get_datelist(path + "/txt_sentoken/neg/")

    test_pos = pos_reviews.review_list[:50]
    test_neg = neg_reviews.review_list[:50]
    test_collection = test_pos + test_neg
    dev_pos = pos_reviews.review_list[50:100]
    dev_neg = neg_reviews.review_list[50:100]
    dev_collection = dev_pos + dev_neg
    training_pos = pos_reviews.review_list[100:]
    training_neg = neg_reviews.review_list[100:]
    training_collection = training_pos + training_neg

    random.shuffle(training_collection)
    random.shuffle(dev_collection)
    random.shuffle(test_collection)
    
    training_set = Reviews(training_collection)
    dev_set = Reviews(dev_collection)
    test_set = Reviews(test_collection)

    return training_set, dev_set, test_set




if __name__ == "__main__":
    print("Preparing data...")
    training_set, dev_set, test_set = load_reviews()

    print('Training classifier...')
    klassifikator = Bayes_classifikator.from_reviews(training_set)

    print('The accuracy of test data is:')
    print(klassifikator.accuracy(test_set))

    print(len(training_set.review_list))


'''
Liebe Studenten,

hier sind meine Kommentare zu Ihrer Übung 3.

Viele Grüße,
 Helmut Schmid



Der Code ist kompliziert und sehr ineffizient.

Das Trainingsprogramm und das Anwendungsprogramm sollten separate
Programme sein:
    klassifikator = Bayes_classifikator.from_reviews(training_set)
    print(klassifikator.accuracy(test_set))


Da die Eingabetexte bereits tokenisiert sind, brauchen Sie keinen Tokeniser:
  from nltk import word_tokenize

Tokeniser und Stoppwortliste aus NLTK zu benutzen ist OK, weil
Tokeniserung und Stoppwort-Entfernung nicht Teil der Aufgabe sind.
FreqDist und ConditionalFreqDist dürfen Sie aber nicht verwenden
  from nltk import FreqDist
  from nltk import ConditionalFreqDist

Soweit ich sehe wandeln Sie hier einfach FreqDist in ein Dictionary um.
   feature_count1 = {k:v for k,v in sorted(feature_count.items(),
key=lambda x:-x[1])}
Das Sortieren erscheint nutzlos.

Statt class_to_number und class_to_prob besser class_freq und class_prob.

statt
        for review in reviews.review_list:
            class_freq[review[0]] += 1
besser
        for clas, wordfreq in reviews.review_list:
            class_freq[clas] += 1

Statt
        class_word_freq = ConditionalFreqDist()
besser
        class_word_freq = defaultdict(lambda: defaultdict(int))

statt vocabulary_dictionary besser word_freq

Hier berechnen Sie die Wortwahrscheinlichkeiten, summieren sie und
werden Sie dann weg:
        prob_unk = 1 - sum([(v-discount0)/sum(word_freq.values()) for v
in word_freq.values()])
Später berechnen Sie dann die Werte noch einmal:
            word_prob_in_all = (self.word_freq[word] -
self.discount0)/sum(self.word_freq.values())
Außerdem berechnen Sie in beiden Fällen die Summe sum(word_freq.values()
immer wieder neu.
Das ist alles sehr ineffizient.

statt
        for i, review in enumerate(reviews.review_list):
            categpry = self.predict_category(review[1])
besser
        for i, (label, word_freq, filepath) in
enumerate(reviews.review_list):
            categpry = self.predict_category(word_freq)

statt
        score_list = {category: self.probability_of_text(feature_counts,
category) for category in self.class_prob.keys()}
        return sorted(score_list.items(), key=lambda x:-x[1])[0][0]
besser
        score_list = {category: self.probability_of_text(feature_counts,
cat) for cat in self.class_prob}
        return max(score_list.items(), key=lambda x:-x[1])[0]

statt
        prob = 0
        for word, num in feature_counts.items():
            prob += self.probability_of_word(word, category)*num
besser
        logprob = 0
        for word, num in feature_counts.items():
            logprob += self.log_probability_of_word(word, category) * num

Sie berechnen hier dem Backoff-Faktor immer wieder neu:
        backoff_factor = 1-sum([self.relative_prob(word, category) for
word in self.class_word_freq[category].keys()])
Das ist alles extrem ineffizient.

Sie sollten so vorgehen:
1) Häufigkeiten extrahieren
2) discount_0 berechnen
3) Korpusgröße berechnen: total = sum(word_freq.values())
4) Wortwahrscheinlichkeiten berechnen:
   word_prob = {word: (freq-discount_0)/total for word, freq in
word_freq.items()}
5) discount_1 berechnen
6) bedingte Wortwahrscheinlichkeiten berechnen
   cond_word_prob = {}
   for clas in word_class_freq:
      total = sum(word_class_freq[clas].values())
      prob = {word: (freq-discount_1)/total for word, freq in
word_class_freq[clas].items()}
      backoff = 1.0 - sum(prob.values())
      cond_word_prob[clas] = {word: prob.get(word,0.0) + backoff*p  for
word, p in word_prob}
Dann speichern Sie die Parameter in einer Datei.
Das Anwendungsprogramm liest dann die Parameter aus der Datei ein.


Punkte: 6



'''











