from naive_bayes1 import Reviews,Bayes_classifikator


#preparing date
training=Reviews.get_datelist('./train/')
dev=Reviews.get_datelist('./dev/')
test=Reviews.get_datelist('./test/')


klassifikator = Bayes_classifikator.from_reviews(training)

def annotation(dateset):
    text_and_label={}
    for inst in dateset.review_list:
        text_and_label[inst[2]]=klassifikator.predict_category(inst[1])
    return text_and_label

def accuracy_of_dateset(dateset):
    accuracy=klassifikator.accuracy(dateset)
    return accuracy


if __name__ == '__main__':
    print('The annotatied label of development date is:')
    devtext_with_label=annotation(dev)
    print(devtext_with_label)
    print('The accuracy of development date is:')
    accuracy_of_dev=accuracy_of_dateset(dev)
    print(accuracy_of_dev)
    print('The annotatied label of test date is:')
    testtest_with_label=annotation(test)
    print(testtest_with_label)
    print('The accuracy of test date is:')
    accuracy_of_test=accuracy_of_dateset(test)
    print(accuracy_of_test)







