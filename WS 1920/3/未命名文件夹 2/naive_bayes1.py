from nltk import word_tokenize
from nltk import FreqDist
from nltk import ConditionalFreqDist
import os
from collections import defaultdict
import json
import math

#reading instances
class Reviews:
    def __init__(self,review_list):
        self.review_list=review_list

    @classmethod #getting all training instances list
    def get_datelist(cls,path):
        daterootlist = os.listdir(path)
        pathlist=[os.path.abspath(path+p) for p in daterootlist if os.path.isdir(path+p)]
        datelist = [p+'/'+p1 for p in pathlist for p1 in os.listdir(p) if os.path.splitext(p1)[1]=='.txt']
        return cls.get_rawtext(datelist)

    @classmethod #reading instances
    def get_rawtext(cls,path_list):
        reviews=[]
        for path in path_list:
            with open (path,'r',encoding = "ISO-8859-1") as f:
                content=f.read()
                features=word_tokenize(content)
                feature_count=FreqDist(features)
                label='neg' if 'neg' in path else 'pos'
                reviews.append((label,feature_count,path))
        return cls(reviews)


class Bayes_classifikator:
    def __init__(self,class_to_number,class_word_to_number,vocabulary_dictionary,discount0,prob_unk):
        self.class_to_number=class_to_number
        self.class_to_prob={c:n/sum(class_to_number.values())for c,n in self.class_to_number.items()}
        self.class_word_to_number=class_word_to_number
        self.vocabulary_dictionary=vocabulary_dictionary
        self.discount0=discount0
        self.prob_unk=prob_unk


    @classmethod
    def from_reviews(cls,reviews):
        class_word_to_number=ConditionalFreqDist()
        class_to_number=defaultdict(int)
        vocabulary_dictionary = defaultdict(int)
        for review in reviews.review_list:
            class_to_number[review[0]]+=1
            for feature,num in review[1].items():
                class_word_to_number[review[0]][feature]+=num
                vocabulary_dictionary[feature]+=num
        n1=0
        n2=0
        for num in vocabulary_dictionary.values():
            if num==1:
                n1+=1
            elif num==2:
                n2+=1
        discount0=n1/(n1+2*n2) if n1+2*n2!=0 else 0
        prob_unk=1-sum([(v-discount0)/sum(vocabulary_dictionary.values())for v in vocabulary_dictionary.values()])
        return cls(class_to_number,class_word_to_number,vocabulary_dictionary,discount0,prob_unk)

    def relative_prob(self,word,category):
        n1 = 0
        n2 = 0
        for num in self.class_word_to_number[category].values():
            if num == 1:
                n1 += 1
            elif num == 2:
                n2 += 1
        discount=n1/(n1+2*n2) if n1+2*n2!=0 else 0
        relative_prob=(self.class_word_to_number[category][word]-discount)/sum(self.class_word_to_number[category]
                                                                               .values())
        return max(0,relative_prob)

    def probability_of_word(self,word,category):
        backoff_factor = 1 - sum(
            [self.relative_prob(word, category) for word in self.class_word_to_number[category].keys()])
        if word in self.vocabulary_dictionary.keys():

          word_prob_in_all = (self.vocabulary_dictionary[word]-self.discount0)/sum(self.vocabulary_dictionary.values())
          word_prob = self.relative_prob(word,category)+backoff_factor*word_prob_in_all
          return math.log(word_prob)
        else:
          return math.log(self.prob_unk*backoff_factor)

    def probability_of_text(self,feature_counts,category):
        prob = 0
        for word,num in feature_counts.items():
            prob += self.probability_of_word(word,category)*num
        return prob+math.log(self.class_to_prob[category])

    def predict_category(self,feature_counts):
        score_list={category:self.probability_of_text(feature_counts,category) for category in self.class_to_prob.keys()}
        return sorted(score_list.items(), key=lambda x:-x[1])[0][0]

    def accuracy(self,reviews):
        correct_num=0
        num=0
        for review in reviews.review_list:
            num+=1
            if review[0]==self.predict_category(review[1]):
                correct_num+=1
        print(num)
        print(correct_num)
        return correct_num/num if num!=0 else 0

    def save_training_parameter(self):
        word_to_prob=ConditionalFreqDist()
        with open ('training_parameter.json','a') as t:
            json.dump(self.class_to_prob,t)
            for category in self.class_to_prob.keys():
               word_to_prob[category]={word:self.probability_of_word(word,category) for word in
                                       self.vocabulary_dictionary.keys()}
            json.dump(word_to_prob,t)

if __name__ == '__main__':
    training=Reviews.get_datelist('./train/')
    training_bayes=Bayes_classifikator.from_reviews(training)
    #training_bayes.save_training_parameter()
    #print(training_bayes.probability_of_word('i','neg'))
    #print(training_bayes.probability_of_word('i', 'pos'))
    #feature_count=FreqDist(word_tokenize('i'))
    #print(training_bayes.predict_category(feature_count))
    #print(training_bayes.class_to_prob)
    #print(training_bayes.class_to_number)
    #print(training_bayes.class_word_to_number.conditions())
    #print(training_bayes.vocabulary_dictionary)
    #print(training_bayes.prob_unk)
    #print(training_bayes.discount0)
    print(training_bayes.accuracy(training))

















